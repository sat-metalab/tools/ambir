#!/usr/bin/env python3
import argparse
import csv

from matplotlib import cm, colors
import matplotlib.pyplot as plt
import numpy as np
import soundfile as sf
import mpl_toolkits.mplot3d as plt3d
import matplotlib.pylab as pl
import pandas as pd
import control as ctl

from direction_of_arrival import reflexion_list as ref_list
from utils import plotting as P
from utils import signal_processing as U

parser = argparse.ArgumentParser(
    description='Generate graph of a first order ambisonic file. Select early '
    'reflections (automatically or manually), and diffuse field regime. Early '
    'reflections are plotted in red, and diffuse field is colored by time of '
    'arrival')
parser.add_argument(
    '-w', '--window_length',
    default=3,
    metavar='',
    type=int,
    help='window length over which to compute means (in samples)')
parser.add_argument(
    '-n', '--reflection_number',
    default=1,
    metavar='',
    type=int,
    help='number of early reflections to select manually')
parser.add_argument(
    '-f', '--file_name',
    required=True,
    metavar='',
    type=str,
    help='ambisonic file name')
parser.add_argument(
    '-a', '--automatic_selection',
    help='automatically detect early reflections in the interval of choice',
    action='store_true',)
parser.add_argument(
    '-e', '--early_reflections_only',
    help='graph only the early reflections',
    action='store_true',)
parser.add_argument(
    '-s', '--skip_window_selection',
    help='skip early reflection window selection',
    action='store_true',)

args = parser.parse_args()


def direction_of_arrival(window_length: int,
                         early_reflexion_number: int,
                         file_name: str,
                         peak_detection: bool,
                         early_reflections_only: bool,
                         skip_window_selection: bool):

    signal, fs = sf.read(file_name)

    RL = ref_list.ReflexionList(U)

    # Get list
    early_reflexion_list = RL.get_early_reflections_list(
        signal, fs, window_length, peak_detection, early_reflexion_number, skip_window_selection)
    if not early_reflections_only:
        diffuse_field_list, first_sample_diffuse = RL.get_diffuse_field_list(signal, fs, window_length)

    # Get data from list
    theta_early, phi_early, module_early_pn, pressure_early, arrival_sample_early = RL.get_data_from_reflexion_list(
        early_reflexion_list)
    if not early_reflections_only:
        theta_diffuse, phi_diffuse, module_diffuse, _, arrival_sample_diffuse = RL.get_data_from_reflexion_list(
            diffuse_field_list)

    arrival_time_early = arrival_sample_early / fs
    # Amplitude Normalization and dB
    pressure_db = ctl.mag2db(np.abs(pressure_early))
    module_early = module_early_pn / np.max(module_early_pn)
    if not early_reflections_only:
        arrival_time_diffuse = arrival_sample_diffuse / fs
        module_diffuse = np.log(module_diffuse / np.min(module_diffuse))
        module_diffuse = module_diffuse / (4 * np.max(module_diffuse))

    csv_file_name = "CSV_" + file_name[3:-4] + ".csv"

    with open(csv_file_name, 'w', newline='') as file:
        writer = csv.writer(file, delimiter='\t')
        writer.writerow(["#", "Sample", "Time", "Module", "Pressure", "Pressure (dB)", "Theta", "Phi"])

        for i in range(len(module_early_pn)):
            writer.writerow([i, arrival_sample_early[i], arrival_time_early[i],
                             module_early_pn[i], pressure_early[i], pressure_db[i], theta_early[i], phi_early[i]])

    df = pd.read_csv(csv_file_name, sep='\t')
    df1 = df.sort_values(by=['Sample'])
    df1.to_csv(csv_file_name, index=False, sep='\t')

    # Plot data
    # Set figure
    fig = plt.figure()
    fig.set_size_inches(10, 10)
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.view_init(azim=120)


    # Plot early reflexions
    if not skip_window_selection:
        x, y, z = P.spherical_to_cartesian(module_early, theta_early, phi_early)

        if early_reflections_only:
            norm2 = colors.Normalize(
                vmin=0,
                vmax=(np.max(arrival_time_early) - np.min(arrival_time_early)) * 1000)
            cmap = cm.hsv
            fig.colorbar(
                cm.ScalarMappable(
                    norm=norm2,
                    cmap=cmap),
                ax=ax,
                shrink=0.8,
                label='delay (ms)')
            color_d = pl.cm.hsv(
                norm2(
                    (arrival_time_early -
                    np.min(arrival_time_early))) * 1000)

            for i in range(np.size(x)):
                line = plt3d.art3d.Line3D((0, x[i]), (0, y[i]), (0, z[i]), c=color_d[i])
                ax.add_line(line)
        else:

            for i in range(np.size(x)):
                line = plt3d.art3d.Line3D((0, x[i]), (0, y[i]), (0, z[i]), c='r')
                ax.add_line(line)

            # Plot diffuse field with colored delay

            start_time_diffuse = first_sample_diffuse / fs

            norm = colors.Normalize(
                vmin=(start_time_diffuse - np.min(arrival_time_early)) * 1000,
                vmax=(start_time_diffuse - np.min(arrival_time_early) + np.max(arrival_time_diffuse)
                      - np.min(arrival_time_diffuse)) * 1000)
            cmap = cm.cool
            fig.colorbar(
                cm.ScalarMappable(
                    norm=norm,
                    cmap=cmap),
                ax=ax,
                shrink=0.8,
                label='delay (ms)')
            color = pl.cm.cool(
                norm(
                    (start_time_diffuse - np.min(arrival_time_early)
                     + arrival_time_diffuse - np.min(arrival_time_diffuse))*1000))
            x, y, z = P.spherical_to_cartesian(
                module_diffuse, theta_diffuse, phi_diffuse)

            for i in range(np.size(x)):
                line = plt3d.art3d.Line3D((0, x[i]), (0, y[i]), (0, z[i]), c=color[i])
                ax.add_line(line)

        plt.show()


if __name__ == "__main__":
    direction_of_arrival(
        args.window_length,
        args.reflection_number,
        args.file_name,
        args.automatic_selection,
        args.early_reflections_only,
        args.skip_window_selection)
