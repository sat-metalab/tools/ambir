#!/usr/bin/env python3

from ambisonic_animation import spherical_harmonics as sh
from matplotlib import cm
from matplotlib.ticker import MaxNLocator
import MidpointNormalize as MN
import argparse
import matplotlib.pyplot as pltt
import numpy as np
import soundfile as sf

parser = argparse.ArgumentParser(
    description='Generate animation of an ambisonic file. Computes the energy'
    ' over a specified window ( using time step) and plots it on the unitary'
    ' sphere. This is done for the desired duration and initial time.')
parser.add_argument(
    '-d', '--duration',
    default=1,
    metavar='',
    help='duration')
parser.add_argument(
    '-i', '--index',
    default=0,
    metavar='',
    help='number of the first png file generated')
parser.add_argument(
    '-t', '--initial_t',
    default=0,
    metavar='',
    help='initial time')
parser.add_argument(
    '-dt', '--time_step',
    default=0.01,
    metavar='',
    help='time step')
parser.add_argument(
    '-p', '--precision',
    default=10,
    metavar='',
    help='Precision')
parser.add_argument(
    '-f', '--Bformat_file_name',
    required=True,
    metavar='',
    help='Bformat file name')
parser.add_argument(
    '-th', '--theta_num',
    default=20,
    metavar='',
    help='azimuthal meshing (number of subdivisions)')
parser.add_argument(
    '-ph', '--phi_num',
    default=10,
    metavar='',
    help='colatitude meshing (number of subdivisions)')
parser.add_argument(
    '-s', '--save_option',
    help='Save instead of show animation',
    action='store_true',)

args = parser.parse_args()


def animate_field(
        duration: float,
        initial_t: float,
        time_step: float,
        Bformat_file_name: str,
        theta_num: int,
        phi_num: int,
        save_option: bool,
        precision: int) -> None:

    # Spherical variables
    phi, theta = np.meshgrid(
        np.linspace(-np.pi / 2, np.pi / 2, phi_num),
        np.linspace(0, 2 * np.pi, theta_num))
    # Signal variables
    coefficient, fs = sf.read(Bformat_file_name)
    initial_sample = int(initial_t * fs)
    sample_step = int(fs * time_step)
    # Animation frame number
    frame_number = int(duration / time_step)

    # precompute spherical harmonics
    order = 3
    spherical_harmonics = np.zeros(
        (order + 1, 2 * order + 1, theta_num, phi_num))
    for degree in range(0, order + 1):
        for index in range(-degree, degree + 1):
            spherical_harmonics[degree, index] = sh.real_spherical_harmonic(
                degree, index, theta, phi)
    energy = np.zeros((theta_num, phi_num, frame_number))
    pressure = np.zeros((theta_num, phi_num, sample_step))
    for frame_index in range(0, frame_number):
        cur_sample = initial_sample + frame_index * sample_step
        print('cur_sample' + str(cur_sample))
        for i in range(0, sample_step - 1, precision):
            pressure[:, :, i] = sh.build_angular_field_acn(
                coefficient[i + cur_sample, :], spherical_harmonics)
        # Compute energy over specified sample steps
        energy[:, :, frame_index] = np.sum(pressure[:, :, :]**2, 2)
    energy = energy / np.max(energy)

    print('Start save png')
    norm = MN.MidpointNormalize(midpoint=0.3, vmin=0, vmax=1.0, clip=True)
    levels = MaxNLocator(nbins=15).tick_values(energy.min(), energy.max())
    fig, ax = pltt.subplots(frameon=False)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('off')
    for frame_index in range(0, frame_number):
        ax.contourf(theta,
                    phi,
                    energy[:,
                           :,
                           frame_index],
                    cmap=cm.jet,
                    levels=levels,
                    norm=norm)
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        fig.savefig(f"{frame_index + np.int(args.index):06d}_animation.png",
                    bbox_inches=extent, dpi=500)


if __name__ == "__main__":
    animate_field(
        np.float(args.duration),
        np.float(args.initial_t),
        np.float(args.time_step),
        args.Bformat_file_name,
        np.uint32(args.theta_num),
        np.uint32(args.phi_num),
        bool(args.save_option),
        int(args.precision))
