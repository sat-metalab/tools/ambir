import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(
    description='Generate graph of main reflexions for goupillon test')
parser.add_argument(
    '-n', '--test_number',
    default=1,
    metavar='',
    type=int,
    help='number of the test to graph')
parser.add_argument(
    '-c', '--channel_number',
    default=48,
    metavar='',
    type=int,
    help='number of reflections in VBAP spatializer')

args = parser.parse_args()


def makespatializer(test_number: int,
                    channel_number: int):

    df = pd.read_csv("MAXCSV_goupillon_test" + str(test_number) + ".csv", sep='\t')

    pressure = df['Pressure'].to_numpy()
    theta = df['Theta'].to_numpy()
    phi = df['Phi'].to_numpy()
    time = df['Time'].to_numpy()

    delay = np.max(time) - time
    ampcomp = np.min(pressure)/pressure

    theta_deg = theta*180/np.pi
    phi_deg = phi*180/np.pi

    filename = "reflectVBAP_test" + str(test_number) + ".scd"

    file1 = open(filename,"w")

    file1.write(f"\
~name = \\reflectVBAP_test{test_number};\n\
~description = \"Main reflections from four goupillons to Zylia\";\n\
~numChannels = {channel_number};\n\
~channelLayout = \\mono;\n\n\
~spk48 = VBAPSpeakerArray.new(3,\n\t[\n")

    for i in range(0, channel_number - 1):
        file1.write(f"\t\t[{theta_deg[i]}, {phi_deg[i]}],\n")

    file1.write(f"\
\t\t[{theta_deg[channel_number - 1]}, {phi_deg[channel_number - 1]}]\n\t]\n);\n\n\
~function = {{ |in = 0, aziDeg = 0, eleDeg = 0, gainDB = -99, delayMs = 1, \
lpHz = 15000, hpHz = 5, spread = 0.01|\n\n\
\tvar gain= gainDB.dbamp;        // convert gainDB to gainAMP\n\
\tvar delay = delayMs * 0.001;   // convert to seconds\n\
\tvar slewDelay = 0.3;           //  note: this needs to be improved ... smoother\n\
\tvar slewGain = 0.1;\n\
\tvar slewFilter = 0.3;\n\
\tvar outsig;\n\
\tvar reflectDelay;\n\
\tvar reflectGain;\n\n\
\treflectDelay = [")

    for i in range(0, channel_number-1):
        file1.write(f"{delay[i]}, ")

    file1.write(f"\
{delay[channel_number-1]}];\n\n\
\treflectGain = [")

    for i in range(0, channel_number-1):
        file1.write(f"{ampcomp[i]}, ")

    file1.write(f"\
{ampcomp[channel_number-1]}];\n\n\
\t// limit cutoff freq range and smooth changes\n\
\tlpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);\n\
\thpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);\n\
\t\n\
\toutsig = in * gain.lag(slewGain);\n\
\toutsig = DelayC.ar(\n\
\t\toutsig,\n\
\t\tmaxdelaytime: 0.5,\n\
\t\tdelaytime: delay.lag(slewDelay)\n\
\t);\n\
\toutsig = LPF.ar(outsig, lpHz);\n\
\toutsig = BHiPass.ar(outsig, hpHz);\n\
\t\n\
\toutsig = VBAP.ar(\n\
\t\tnumChans: ~spk48.numSpeakers,\n\
\t\tin: outsig,\n\
\t\tbufnum: ~vbuf48.bufnum,\n\
\t\tazimuth: aziDeg.circleRamp,\n\
\t\televation: eleDeg.circleRamp,\n\
\t\tspread: spread\n\
\t);\n\
\t\n\
\tDelayN.ar(outsig, 3.5, reflectDelay) * reflectGain;\n\
}};\n\
\n\
~setup = {{ |satieInstance|\n\
\t~vbuf48 = Buffer.loadCollection(satieInstance.config.server, ~spk48.getSetsAndMatrices);\n\
}};\n")

    file1.close()

if __name__ == "__main__":
    makespatializer(
        args.test_number,
        args.channel_number)

