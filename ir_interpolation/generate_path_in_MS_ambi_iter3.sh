#!/bin/bash

python3 interpolate_irs.py -4 14 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py -2 14.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 0 14.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 2 14.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 3.5 14 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 5 13.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 6.5 12.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 6.5 11 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 9 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 7.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 5.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 3.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 1.5 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 0 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7 -2 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7.5 -4 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv
python3 interpolate_irs.py 7.8 -6 1 use_cases/maison_symph_ambi_iter3/maison_symph_ambi_iter3.csv