import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import numpy as np
from scipy.fftpack import fft


def db_amp(y: np.ndarray) -> np.ndarray:
    x = np.ndarray.copy(y)
    x = 10 * np.log10(x / np.max(x))
    return x


def select_chunk(signal: np.ndarray) -> tuple:
    fig = plt.figure(figsize=(11, 7))
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(signal)
    plt.ylabel('Value')
    plt.xlabel('Sample')
    cursor = Cursor(ax, useblit=True, color='k', linewidth=1)
    zoom_ok = False
    print('\nZoom or pan to view, \npress spacebar when ready to click:\n')
    while not zoom_ok:
        zoom_ok = plt.waitforbuttonpress()
    print('Select starting point: ')
    val = plt.ginput(1)
    zoom_ok = False
    print('\nZoom or pan to view, \npress spacebar when ready to click:\n')
    while not zoom_ok:
        zoom_ok = plt.waitforbuttonpress()
    print('Select ending point: ')
    val2 = plt.ginput(1)
    plt.close()
    return signal[np.int(val[0][0]):np.int(val2[0][0])], np.int(val[0][0])


def select_window(signal: np.ndarray, window_length: int) -> np.ndarray:

    fig = plt.figure(figsize=(11, 7))
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(signal[:, 0])
    plt.ylabel('Value')
    plt.xlabel('Sample')
    cursor = Cursor(ax, useblit=True, color='k', linewidth=1)
    zoom_ok = False
    print('\nZoom or pan to view, \npress spacebar when ready to click:\n')
    while not zoom_ok:
        zoom_ok = plt.waitforbuttonpress()
    print('Select starting point: ')
    val = plt.ginput(1)
    center_sample = val[0][0]
    delta_sample = int(window_length / 2)
    plt.close()
    return signal[int(center_sample - delta_sample):int(center_sample + delta_sample)], val[0][0]


def signal_to_noise_ratio(signal: np.ndarray) -> float:
    print('Select noise chunk')
    noise = select_chunk(signal)
    print('Done selecting noise chunk')
    print('Select signal chunk')
    signal = select_chunk(signal)
    print('Done selecting signal chunk')
    snr = 10 * np.log10(np.mean(signal**2) / np.mean(noise**2))
    print('SNR=' + str(int(snr)) + 'db')
    return snr


def spectrum(signal: np.ndarray, fs: int) -> [np.ndarray, np.ndarray, np.ndarray]:
    complex_spectrum = fft(signal)
    amplitude = abs(complex_spectrum)
    phase = np.unwrap(np.angle(complex_spectrum))
    frequency_axis = np.linspace(0, fs / 2, np.shape(signal)[0] // 2)
    amplitude_db = db_amp(amplitude[np.shape(amplitude)[
                          0] - (np.shape(amplitude)[0] // 2) - 1:-1])
    return frequency_axis, amplitude_db, phase


def lee_correlation(signal: np.ndarray) -> np.ndarray:

    print('Select early reflexions regime')
    P = select_chunk(signal)
    print('Select direct sound')
    Pdirect = select_chunk(signal)
    correlation = np.correlate(P[:, 0], Pdirect[:, 0], 'same')
    print(np.shape(correlation))
    correlation = np.reshape(correlation, (np.shape(correlation)[0], 1))
    correlation = np.concatenate((correlation, P[:, 0:-1]), 1)

    return correlation


def lee_correlation_4D(signal: np.ndarray) -> [np.ndarray, np.ndarray]:

    w = signal[:, 0]
    print('Select early reflexions regime')
    P = select_chunk(signal)
    print('Select direct sound')
    Pdirect = select_chunk(w)
    zeropad = Pdirect
    correlation0 = np.correlate(P[:, 0], zeropad, 'same')
    correlation1 = np.correlate(P[:, 1], zeropad, 'same')
    correlation2 = np.correlate(P[:, 2], zeropad, 'same')
    correlation3 = np.correlate(P[:, 3], zeropad, 'same')
    correlation = np.array(
        [correlation0, correlation1, correlation2, correlation3])
    correlation = np.transpose(correlation)

    return correlation, P


def time_average(signal: np.ndarray, sample_window: int) -> np.ndarray:
    sample_total = np.shape(signal)[0]
    window_number = int(sample_total / sample_window)
    average = np.zeros(window_number)
    for i in range(window_number):
        average[i] = np.nanmean(
            signal[i * sample_window:(i + 1) * sample_window])
    return average
