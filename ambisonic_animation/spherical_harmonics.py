#!/usr/bin/env python3
import numpy as np
from scipy.special import lpmv


def real_spherical_harmonic(degree: int, index: int, theta: float, phi: float) -> np.ndarray:

    if index < 0:
        trig = np.sin(-index * theta)
    else:
        trig = np.cos(index * theta)

    N = n3d(degree, index)

    cs_phase = (-1)**index
    associated_legendre_polynomial = lpmv(np.abs(index), degree, np.sin(phi))
    spherical_harmonic = N * cs_phase * associated_legendre_polynomial * trig

    return spherical_harmonic


def sn3d(degree: int, index: int):

    if index == 0:
        delta = 1
    else:
        delta = 0

    index = np.abs(index)

    N = np.sqrt((2 - delta) * (np.math.factorial(degree - index)) /
                (np.math.factorial(degree + index)))

    return N


def n3d(degree: int, index: int):
    N = np.sqrt(2 * degree + 1) * sn3d(degree, index)
    return N


def build_angular_field_acn(coeff: np.ndarray, spherical_harmonics: np.ndarray) -> np.ndarray:
    order = int(np.sqrt(np.shape(coeff)[0])) - 1
    field = 0
    for degree in range(0, order + 1):
        for index in range(-degree, degree + 1):
            acn = degree * (degree + 1) + index
            spherical_harmonic = spherical_harmonics[degree, index]
            coefficient = coeff[acn]
            field += coefficient * spherical_harmonic * (2 * degree + 1)
    return field
