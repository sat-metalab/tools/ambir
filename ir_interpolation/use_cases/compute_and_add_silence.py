import os
import soundfile as sf
import numpy as np
import sys
sys.path.insert(1,os.path.abspath("../"))
from ir_config import Source
from ir_config import Mic
from typing import List

def insert_silence(originial_array: np.array, distance: float) -> np.array :
    """ A method to add silence in mono files. 

    Args:
        originial_array (np.array): The array representing the IR.
        distance (float): Distance of the mic to the Source. 

    Returns:
        np.array: The original array with the correct number of silent frames before the beginning of the IR. 
    """

    delay_time = distance / 344
    silence = np.zeros(int(delay_time * samplerate))

    data = np.array(len(silence) + len(originial_array))
    data = np.append(silence,originial_array)

    return data

def insert_silence_in_ambisonic(
    originial_array: np.ndarray, distance: float, number_of_channels: int, number_of_frames: int
) -> np.array :
    """ A method to add silence before the start of the IR, depending on the distance of the mic to the source.

    Args:
        originial_array (np.ndarray): Array where to add silence. 
        distance (float): Distance of Mic to Source
        number_of_channels (int): number of channels of the IR
        number_of_frames (int): number of frames of the IR.

    Returns:
        np.array: The original array with the correct number of silent frames before the beginning of the IR. 
    """
    
    delay_time = distance / 344
    silence = np.zeros(int(delay_time * samplerate))

    data = np.ndarray((number_of_channels, number_of_frames + len(silence)))

    for channel in range(number_of_channels):
        data[channel] = np.append(silence,originial_array[channel])

    return data

# for each sources, create a Source instance with coordinates in space depending on the location of the speakers.
sources : List[Source] = []

sources.append(Source("S01", (0, 12, 1)))
sources.append(Source("S02", (4, 12.5, 1)))
sources.append(Source("S03", (6.5, 15.5, 1)))
sources.append(Source("S04", (0, 16, 1)))
sources.append(Source("S05", (-6.5, 15.5, 1)))
sources.append(Source("S06", (-4, 12.5, 1)))
sources.append(Source("S07", (7, 10.5, 1)))
sources.append(Source("S08", (7, 6.5, 1)))
sources.append(Source("S09", (7, 2.5, 1)))
sources.append(Source("S10", (7, -3, 1)))
sources.append(Source("S11", (8, -7, 1)))

# # for each irs in session 2, create a Mic instance with coordinates in space depending on the location of the mic used.
# irs_session2 : List[Mic] = []

# directory_session2 = "edited_irs_MS/session_2/"
# abs_path_to_dir_2 = os.path.abspath(directory_session2)

# for filename in os.listdir(abs_path_to_dir_2):
    
#     mic_number = filename.split('h')[1].split('-')[0]

#     if mic_number == '07':
#         irs_session2.append(Mic(filename, (7,13,1)))
#     elif mic_number == '09':
#         irs_session2.append(Mic(filename, (0,8,1)))
#     elif mic_number == '16':
#         irs_session2.append(Mic(filename, (-6,15,1)))
#     elif mic_number == '17':
#         irs_session2.append(Mic(filename, (0,14,1)))
#     elif mic_number == '21':
#         irs_session2.append(Mic(filename, (-7,13,1)))
#     elif mic_number == '22':
#         irs_session2.append(Mic(filename, (6,15,1)))
#     elif mic_number == '19':
#         irs_session2.append(Mic(filename, (0,0,1)))


# # for each irs in session 2, compute to the distance to its source.
# for ir in irs_session2:
#     for source in sources:
#         if ir._a_file.split('S')[1].split('_')[0] == f"{source._a_file[1]}{source._a_file[2]}":
#             ir.get_distance_with(source) 
#             #print (f"Session 2: File {ir._a_file} : {ir._distance_to_source}")

# for ir in irs_session2:
    
#     # reading the file and storing it in an np.ndarray 
#     output, samplerate = sf.read(abs_path_to_dir_2+"/"+ir._a_file)
    
#     data = insert_silence(output, ir._distance_to_source)

#     new_dir = "edited_irs_MS/session_2_timed/"
#     abs_path_new_file_sess2 = os.path.abspath(new_dir)
#     new_name = abs_path_new_file_sess2 +"/"+ ir._a_file

#     sf.write(new_name, data, samplerate)

# print("Done for session 2.")




# for each irs iteration create a Mic instance with coordinates in space depending on the location of the used mic.
irs_iteration : List[Mic] = []

dir_iteration = "ambisonic_irs_MS/iteration3_croped"
abs_path_to_iter = os.path.abspath(dir_iteration)

for filename in os.listdir(abs_path_to_iter):
    irs_iteration.append(Mic(filename, (0, 12.5, 1.6)))

# for each irs in iteration2, compute to the distance to its source.
for ir in irs_iteration:
    ir_mic = ir._a_file.split('up_')[1].split('_edited')[0]
    if len(ir_mic) == 1:
        ir_mic = '0'+ ir_mic
    for source in sources:
        if ir_mic == f"{source._a_file[1]}{source._a_file[2]}":
            ir.get_distance_with(source) 
            print (f"Iteration: File {ir._a_file} : {ir._distance_to_source}")

# for each ir in iteration, add silent frames 
for ir in irs_iteration:
    
    # reading the file and storing it in an np.ndarray 
    output, samplerate = sf.read(abs_path_to_iter+"/"+ir._a_file)
    output = output.T
    number_of_channels, number_of_frames = output.shape

    data = insert_silence_in_ambisonic(output, ir._distance_to_source, number_of_channels, number_of_frames)

    new_dir = "ambisonic_irs_MS/iteration3_timed/"
    abs_path_new_dir = os.path.abspath(new_dir)
    new_name = abs_path_new_dir +"/"+ ir._a_file

    sf.write(new_name, data.T, samplerate)

print("Done for this iteration.")

