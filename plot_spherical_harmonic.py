import argparse
import numpy as np
from ambisonic_animation import spherical_harmonics as sh
from utils import plotting as plt

parser = argparse.ArgumentParser(
    description='Compute and plot a spherical harmonic')
parser.add_argument(
    '-l',
    default=1,
    metavar='',
    help='degree')
parser.add_argument(
    '-m',
    default=1,
    metavar='',
    help='index')
parser.add_argument(
    '-t',
    default=100,
    metavar='',
    help='azimuthal meshing')
parser.add_argument(
    '-p',
    default=50,
    metavar='',
    help='colatitude meshing')

args = parser.parse_args()


def plot_spherical_harmonic(degree, index, theta_num, phi_num):
    phi, theta = np.meshgrid(
        np.linspace(-np.pi / 2, np.pi / 2, phi_num),
        np.linspace(0, 2 * np.pi, theta_num))
    sph = sh.real_spherical_harmonic(degree, index, theta, phi)
    sph = sph / np.max(sph)
    plt.spherical_plot(sph, theta, phi, 2, r'$\Psi_' +
                       str(degree) + '^{' + str(index) + r'}(\\theta ,\phi )$')


if __name__ == "__main__":
    plot_spherical_harmonic(
        np.uint32(
            args.l), np.uint32(
            args.m), np.uint32(
                args.t), np.uint32(
                    args.p))
