from .reflexion import Reflexion as R
import numpy as np
import scipy.signal as ssig
import matplotlib.pyplot as plt


class ReflexionList():
    def __init__(self, signal_proc_cls):
        self.u = signal_proc_cls
        self.early_reflexion_list = []
        self.diffuse_field_list = []

# For getting a reflexion manually

    def add_reflexion_click(self, data, reflexion_list, window_length):
        signal, arrival_sample = self.u.select_window(data, window_length)
        ref = R(signal, int(round(arrival_sample)))
        reflexion_list.append(ref)

# For getting a reflexion by sample

    def add_reflexion_sample(self, arrival_sample,
                             data, reflexion_list, window_length):
        if window_length % 2 == 0:
            delta_window = int(window_length / 2)
            signal = data[arrival_sample -
                          delta_window:arrival_sample +
                          delta_window, :]
            ref = R(signal, arrival_sample)
            reflexion_list.append(ref)
        else:
            delta_window = int(window_length / 2)
            signal = data[arrival_sample -
                          delta_window:arrival_sample +
                          delta_window + 1, :]
            ref = R(signal, arrival_sample)
            reflexion_list.append(ref)

# Ask each reflection in the list for a specific parameter. Other parameters
# computed by reflexion.py should be added to this function (diffusivity or
# energy density)

    def get_data_from_reflexion_list(self, reflexion_list):
        theta = []
        phi = []
        module = []
        pressure = []
        arrival_sample = []

        for i in reflexion_list:
            theta.append(i.get_doa_mean()[0])
            phi.append(i.get_doa_mean()[1])
            module.append(i.get_intensity_modulus())
            pressure.append(i.get_pressure())
            arrival_sample.append(i.arrival_sample)

        theta = np.array(theta)
        phi = np.array(phi)
        module = np.array(module)
        arrival_sample = np.array(arrival_sample)

        return theta, phi, module, pressure, arrival_sample

# Fill a list with early reflexions

    def get_early_reflections_list(
            self,
            signal,
            fs,
            window_length,
            peak_detection=True,
            early_reflexion_number=2,
            skip_window_selection=0):

        early_reflexion_list = []

        if not peak_detection:
            count = 0
            print('\n')
            print('Select ' + str(early_reflexion_number) + ' reflexions')
            for i in range(early_reflexion_number):
                self.add_reflexion_click(
                    signal, early_reflexion_list, window_length)
                count += 1
                print('reflexion added,  ' +
                      str(early_reflexion_number - count) + ' to go')
        else:
            if not skip_window_selection:
                omni, first_sample_early = self.u.select_chunk(signal)
            else:
                omni = signal[425000: 475000]  # The end sample value may need to be adjusted for large rooms
                first_sample_early = 425000
            omni = omni[:, 0]
            peaks, _ = ssig.find_peaks(abs(omni), height=(0.25 * max(omni)), distance=6)  # Adjust peak detection values here
            xi = range(first_sample_early, first_sample_early + len(omni))
            '''
            plt.figure()
            plt.plot(xi, omni)
            plt.plot(first_sample_early + peaks, omni[peaks], 'x')
            '''

            for i in peaks:
                self.add_reflexion_sample(
                    first_sample_early + i, signal, early_reflexion_list, window_length)
        return early_reflexion_list

# Fill a list with diffuse field time windows

    def get_diffuse_field_list(self, signal, fs, window_length):
        print('\n')
        print('Select diffuse field regime')
        diffuse_field_list = []
        diffuse_field, first_sample_diffuse = self.u.select_chunk(signal)
        windows_number = int(np.shape(diffuse_field)[0] / window_length)

        for i in range(windows_number):
            arrival_sample = i * window_length + int(window_length / 2)
            self.add_reflexion_sample(
                arrival_sample,
                diffuse_field,
                diffuse_field_list,
                window_length)
        return diffuse_field_list, first_sample_diffuse
