# Measured Impulse Response Interpolation Tool (MIRIT)

MIRIT is a Python script to interpolate measured room impulse responses (RIRs). It is part of the Ambir library. The idea of the algorithm is to compute intermediate measures in space from a (relatively) small set of measures, in order to simulate the acoustic of a whole room and to derive an immersive experience from a set of recorded RIRs. The tool supports multichannel RIRs (for example ambisonic). 
In order to interpolate RIRs, the approach is to use dynamic time warping (DTW) to align reflections of each RIRs prior to the interpolation process. The DTW being fairly costly, a multiprocessing implementation has been chosen to speed up the process.

## Installation 

Use the package manager pip to install all required dependencies:

```bash
pip install -r requirements.txt
```

## Usage

Run the following command to get a sense on how to use the script:

```bash
python3 interpolate_irs.py -h
```

In order to use the script, one must first create a CSV file to specify the paths and the coordinates in space of all measured RIRs files. This is achieved using the following CSV format: 

```
FILE, X-COORD, Y-COORD, Z-COORD
/path/to/ir_1, x_1, y_1, z_1
/path/to/ir_2, x_2, y_2, z_2
/path/to/ir_3, x_3, y_3, z_3
/path/to/ir_4, x_4, y_4, z_4
...
```
(Examples can be found under use_cases/centech/centech_IR.csv and use_cases/maison_symph_S08/maison_symph_S08.csv)

The general usage is as follows:

```bash
python3 interpolate_irs.py X_p Y_p Z_p file_with_RIRs_info.csv
```

One can specify the following arguments:
- the x-y-z coordinates of the wanted point that we want to compute 
- the CSV file name.
- optional parameters: 
    - '-p' to plot the generated RIR on top of the selected RIRs
    - '-m' to plot the matching achieved by the DTW

NOTE: The interpolation is achieved using 3 points. Hence if the wanted point is not at least between 3 points, an error will be thrown. 

NOTE: Ploting the dtw-matching will stop the execution. 

An example of interpolation between mono RIRs (captured in the Maison Symphonique of Montreal by the Metalab):

```bash
python3 interpolate_irs.py 4 10 1 use_cases/maison_symph_S08/maison_symph_S08.csv -p
```

An example of interpolation between 3rd degree ambisonic RIRs (captured in centech in Montreal by the Metalab):

```bash
python3 interpolate_irs.py 0.5 0.5 1 use_cases/centech/centech_IR.csv -p
```

## Editing RIRs prior to interpolation

It is strongly recommended that you edit the RIRs before running the script, such that the direct sound of the RIR begins during at most the first second of the recording, and that it ends when the tails ends. Removing the silent frames from the recording (namely before and after the actual impulse response) allows for a better performance and avoids errors with the dynamic time warping function. 

One can edit the RIRs manually, but a Python script is available under use_cases/remove_silence.py. It allows you to remove those silent frames. Inside the script, one can specify the silence threshold, as well as the ratio of silent frames used to determine the begining of the IR. Feel free to edit the variable to your needs. 

A more intuitive and easier script to crop RIRs can also be found under use_cases/crop_recordings.py. Here, one only need to specify the starting time, the ending time and the files to edit. 

## Credits 

This method has been largely inspired by the paper "Dynamic Time Wapring for Acoustic Response Interpolation: Possibilities and Limitations" written by G. Kearney, C. Masterson, S. Adamas and F. Boland (2009).

This method is using the dtw-python library, credits to: Giorgino. Computing and Visualizing Dynamic Time Warping Alignments in R: The dtw Package. J. Stat. Soft., 31 (2009)

## Authors 

Hector Teyssier 

## Sponsors

This project is made possible thanks to the [Society for Arts and Technology](http://www.sat.qc.ca) (also known as SAT) in Montreal.

## License

LGPL 
