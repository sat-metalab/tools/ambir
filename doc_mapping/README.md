# Using AmbIR

### Energy Map

__Obtaining an ambisonic animation :__

The following script generates a 96 seconds animation. It runs 96 times the energy\_equi\_to\_images.py, using the `-i` option in order to generate the correct image index. The `-dt` option corresponds to the duration between two subsequent frames; in this case we are generating a 30fps video.
```
for i in `seq 0 1 96`; do python3 ~/src/ambir/energy_equi_to_images.py -i $(($i * 30)) -d 1 -t $i -dt 0.03333333333333 -p 1 -f 3rdOrderAmbisonics_ACN.wav -th 180 -ph 90 -s; done
```

With ffmpeg you can encode your image sequence into a mp4 file:
```
ffmpeg -r 30 -i %06d_animation.png -vf "crop=3199:2399, scale=3200:1600" -c:v h264 -preset slow -b:v 25M res.mp4
```

Then, audio can be added to the generated video. To do so, export a stereo mix of the original recording (before B-format conversion), e.g. with Audacity, then use the following command:
```
ffmpeg -i res.mp4 -i stereo_audio_file.mp3 -codec copy -shortest res_with_audio.mp4
```

__Results__

Screenshots from generated video, displaying the energy from a clap on one side of the Zylia and on the opposite side.

![First clap](energymap.png)
![Opposite side clap](energymap2.png)


## Hedgehog Pattern (DOA) graph

__Steps for generating an Ambisonic Impulse Response (AIR) file:__

* Generate an exponential sine sweep (ESS) and its associated filter:

python3 gen_ess_2.py -ns ess_file_name.wav -nf filter_file_name.npy
Note: a sweep and filter should already exist, named ess.wav and filter.py

* Play the ESS in the room and make an ambisonic recording:
```
arecord -Dhw:CARD=ZM13E,DEV=0 -N -c19 -t wav -f S24_3LE -r 48000  -v -V mono recorded_ess.wav
```
* Convert the ambisonic recording to B-format (Zylia Ambisonic Converter, ACN-SN3D 1st order):
```
zylia-ambisonics-converter --channelOrdering ACN --input recorded_ess.wav --output B_recorded_ess.wav --device ZM-1-3E
```
* Deconvolve the recording
```
python3 farina_deconv.py -r B_recorded_ess.wav -f filter_file_name.npy -i ir_file_name
```
* Trim, compute SNR and compute spectrum are defined in utility.py (WIP)

__Steps for generating a Hedgehog plot :__

* If the number of early reflections to be analyzed is known, use the -n flag. 

```
python3 direction_of_arrival_plot.py -f AIR_file_name.wav -w window_length -n early_reflection_number
```

* Navigate mapltolib's figure and when ready to select an early reflexion, press space and than click at the desired sample.

* When done selecting early reflections, select the interval to analyze as diffused field. Press space and then click on the desired coordinate twice, in order to chose the beginning and end of the interval respectively.

* It is possible to detect early reflections with Scypy's peak detection, using -a.

```
python3 direction_of_arrival_plot.py -f AIR_file_name.wav -w window_length -a
```

* Pressing space and then click twice, select an interval over which to detect the peaks.

* Do the same for the diffuse field.


__Results :__

Using the following speaker/microphone setup:

![Front](zyliafront.jpg)

![Back](zyliaback.jpg)

Ambisonic impulse response (AIR) wav files were obtained, first with both speakers playing the sine sweep, then with only the left one (looking at the rear of the speakers). Both files were then analyzed and compared using direction_of_arrival.py


With both speakers:

![With both speakers](DOA_2speakers.png)

Turning off the right speaker:

![Turning off the right speaker](DOA_1speaker.png)

The red lines represent early specular reflections, and the rest are diffuse field reflections.

It appears that some of the reflections coming from the right side wall disappeared. The remaining ones increased in size, which could be explained by destructive interference in the initial (two-speaker) sweep. More testing would be required to test that hypothesis.
