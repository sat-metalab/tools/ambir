# Setup

![Full setup](./pictures/setup.JPG)

![Goupillon 1](./pictures/Goupillon1.JPG)

![Goupillon 2](./pictures/Goupillon2.JPG)

![Goupillon 3](./pictures/Goupillon3.JPG)

![Goupillon 4](./pictures/Goupillon4.JPG)

![Goupillon 5](./pictures/Goupillon5.JPG)

![Reflector panels](./pictures/reflectors.JPG)

# Observations

The first prototype has been wired with inverted phase on channels 1 and 8, and tests were done with the presumption that it would not affect results significantly. It may be worth asking what would be the result of having inverted phase on opposite drivers.

Possibility for improving the VBAP spatializer is limited because it doesn't allow for a number of reflections higher than 48. This does not provide a good coverage of the sphere area, as some reflections are closer to one another, leaving "blind spots" at some angles. Moreover, hinders the possibility of considering more than one reflection per channel.

One should keep in mind that playing an impulse to emulate a reflection at the sweet spot will also generate other reflections as well as a direct sound.

Playing an impulse on each channel one at a time sounds more evenly distributed when not adjusting for reflection delay. One may hypothesize that this is because the direct sound is perceived first, and the time between the direct sound and reflection is short enough that they seem like a single sound event.

The sweep test results are not extremely consistent between repetitions; they were performed during the night to limit ambient noise, but this may still be a factor. The poor low-frequency response of the drivers may also be an influence.

Resonance in the dodecahedron speaker is significant; when playing a sufficiently loud sound from one channel, the channel directly opposite will
vibrate enough to be perceptible, and the other channels will do so as well to a lesser extent. This limits the potential for directed sound beams. It may be worth
considering adding some dampening material inside of the wooden enclosures.

The protective covers' effect on frequency response was not considered. During testing the fifth speaker had its covers installed, but the others were being manufactured. In theory they should have a certain level of influence on higher frequencies.

For further prototyping, it may be more efficient to use 3D printing to manufacture the enclosure, and perhaps it would allow for a bigger speaker. The printers at the CIRMMT laboratory may be worth considering.

# Additional notes

The original window length for calculating mean values caused it to cover a plurality of peaks, some positive and some negative, which results in a meaningless mean value. Even with the window shortened to three samples (one before and one after the detected peak), questions remain about the purpose of doing so, and how a wide and a narrow peak should be distinguished.

A previous version of farina_deconv.py would normalize the impulse responses by their maximum amplitude. This has been modified in order to be able to compare IR's between one another. To do so, the IR values are all attenuated by a factor of 1000 to prevent clipping. This value was determined by playing a sweep at maximum volume at a few inches from the Zylia microphone. However, it may necessitate adjustments for different setups.

# sweeptest.sh

## Description

The bash script outputs a sine sweep on a single Goupillon channel, and simultaneously records with the Zylia ambisonic microphone.

The resulting 19 channel recording is then converted to a B-format (1st order ACN-SN3D), which is then deconvoluted using Farina.py to obtain an Ambisonic Impulse Response (AIR) file. This processed is repeated for each driver, and produces 12 AIR files per speaker.

The AIR files are then analyzed, and the reflections are compiled in a CSV file for each channel.

The main reflections, one for each CSV file, are then compiled into another CSV file.

This final CSV file is then used to write a SATIE VBAP spatializer.

The original recordings, B-format files, AIR files, CSV files and spatializer file are automatically moved to a subfolder created for them. These should be removed between each set of tests to avoid overwriting.

## Method

0. You must have the following installed:
	* Zylia microphone driver (https://www.zylia.co/zylia-zm-1-drivers.html)
	* Zylia Ambisonics Converter (https://www.zylia.co/zylia-ambisonics-converter-download.html)
	* QJackCtl (```sudo yum install qjackctl``` or ```sudo dnf install qjackctl```)
	* Python modules: Matplotlib, Scipy, Soundfile, Pandas, Control, Argparse

	| WARNING: The outputs are muted by default on startup; make sure to launch HDSPMixer, which will automatically unmute them |

1. Connect the Zylia and the DAC that will output to the speaker

2. In Jack (Setup...), configure and save a preset for the Zylia microphone, and name it zylia
	* Realtime On
	* Sample Rate = 48000
	* Frames/Period = 1024
	* Periods/Buffer = 2

3. In Jack (Setup...), configure a preset for the DAC outputting to the speaker
	* Realtime On
	* Sample Rate = 48000
	* Frames/Period = 1024
	* Periods/Buffer = 2

4. Start the Jack server using the DAC preset

![Step 1](./pictures/connections1.png)

5. Open a terminal, and enter the following command:

```
alsa_in -j zylia -d hw:ZM13E -c 19
```

![Step 2](./pictures/connections2.png)

6. Open another terminal, and launch the bash script sweeptest.sh
	* navigate to bash script directory
	* ``` ./sweeptest.sh -n # -s # -v # -d #```
	* Parse arguments: number of tests (-n), number of speakers (-s), volume of sweep playback (-v, in DBFS) and delay befaure launching tests (-d, in seconds by default, but suffix m or h can be used for minutes or hours)


![Step 3](./pictures/connections3.png)

## direction_of_arrival_plot.py

This script analyzes the AIR file generated by farina_deconv.py, identifying reflections through peak detection of the impulse response's amplitude.

Parse arguments:
	* -w: Window length for calculating means (in samples)
	* -n: Number of reflections to select (only for manual selection mode)
	* -f: Ambisonic file name
	* -a: Automatic peak detection mode (select beginning and end of detection window)
	* -e: Plot only the early reflections
	* -s: Skip window selection for early reflections (selects a window large enough to cover the whole impulse response, which removes the need for user input)

	This generates a plot of the reflections (unless option -s is selected), and a CSV file detailing parameter values (pressure, theta, phi, etc.) for each reflection.

![Peaks plot](./pictures/Peaks_1.png)


## makespatializer.py

Takes the peak reflection CSV file MAXCSV_goupillon_test#.csv and generates the spatializer reflectVBAP_test#.scd

Parse arguments: number of channels (-c) and test number (-n). [These values are set automatically when using sweeptest.sh]

## graphreflections.py

Takes a MAXCSV_goupillon_test#.csv file and generates a 3D plot of the main reflections listed. A color spectrum is used to illustrate their distribution in time.

![Maximum peaks plot - top view](./pictures/Maxpeaks_1.png)
![Maximum peaks plot - side view 1](./pictures/Maxpeaks_2.png)
![Maximum peaks plot - side  view2](./pictures/Maxpeaks_3.png)

# SuperCollider tests

The underlying hypotheses for these tests are the following:
- If the amplitude of the main reflection for each speaker is adjusted by a factor inversely proportional to the pressure measured by the Zylia microphone, then all the main reflections should have the same amplitude at the sweet spot (the Zylia);
- If the main reflection for each speaker is delayed by the amount of time between itself and the last reflection, then all the main reflections should arrive at the same time at the sweet spot (the Zylia).

## 60channel_impulse.scd

This script generates an impulse on each channel, either simultaneously or with a delay between each, adjusting amplitude and delay based on a CSV file containing data for the max reflections (MAXCSV_goupillon_test#.csv).

Someone who stands at the position of the Zylia microphone (position during the sweep test associated to the CSV file) should hear a single sound event, or a series of impulses evenly spaced in time.

Playback can be set to loop to avoid having to trigger repeatedly during testing.

Results: The sweet spot is somewhat audible when it comes to evenness of volume, but a wider speaker distribution may be necessary to assess the efficacy of the delay compensation. As it is, the speakers are relatively close to the sweet spot, and the time difference between a direct sound and its main reflection is of the order of a few milliseconds.


## 60channel_sweepupdown.scd

This script generates a sound (by default a short beep) on each channel, one at a time, with variation of frequency. The amplitude is compensated through the CSV file data, but also to attenuate higher frequencies to have equal perceived volume across the frequency spectrum.

Someone who stands at the position of the Zylia microphone (position during the sweep test associated to the CSV file) should perceive a frequency sweep of even volume, with a varying direction of arrival.

The frequency response of the goupillon is not taken into account, so the range of frequency selected for the sweep should fall within the most linear portion of the drivers FR.

## scramble.scd

This script a sound (by default a short beep) on each channel, on at a time, with increasing frequency, with the order of the channels randomized.

The delay and amplitude compensation has not been implemented; it would require the matrices for both adjustments to be shuffled identically as the output channel matrix.

## reflect.scd

This script runs with reflectVBAP_test#, the spatializer generated by makespatializer.py through the sweeptest.sh script.

Provided the spatializer has been added to SATIE's spatializer plugin folder, it can be launched, and sounds can be instantiated.

Th Supercollider interpreter must be rebooted after adding the spatializer to SATIE in order for the spatializer to be accessible.

Limitation:
- Manual addition of the spatializer to SATIE is required;
- sounds set in an azimuth/elevation without any reflection will be played from a different angle (from the speaker(s) closest to that angle);
- Depending where the sound originates, its playback will occur on a different number of speakers, altering the sound's spread;
- It is not possible to increase the number of reflections above 48, which limits the setup to four 12-channel speakers.

## rotate.scd

This script runs with the dodecahedronVBAP spatializer, and allows an instance of sound to be rotated continuously around the azimuth.

Currently, the azimuth is set to 0 at the beginning of the process. Ideally, the initial azimuth value should be set to be the value prior to starting the rotation, so there is no discontinuity.

## stereo_sweepupdown.scd

This script is analogous to 60channel_sweepupdown. The 60 channels are represented by 60 panning levels between -1 (hard left) and 1 (hard right). The purpose of this script is to audition tests for the goupillon without having access to the speakers.
